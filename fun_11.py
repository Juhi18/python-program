# design a recursive function factorial to return factorial of given number.


def fact(n):

    if n >= 1:
        f = n*fact(n-1);
        return(f);
    else:
        return(1);


print(fact(5));
print(fact(7));
