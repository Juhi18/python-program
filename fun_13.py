# design a recursive function SOD to return SUM OF DIGIT of given number.


def SOD(n):

    if n > 1:
        f = n+SOD(n-1);
        return f;
    else:
        return 1


print(SOD(5));
print(SOD(7));
