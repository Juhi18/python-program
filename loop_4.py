# WAP to read a number and  find sum of all number from 1 to that number

num = int(input("enter the number : "));

output = "";
total = 0;

for x in range (1,num+1):
    output = output + str(x);
    if x != num:
        output = output + " + ";
    total = total + x;

print("{} = {}".format(output,total));
