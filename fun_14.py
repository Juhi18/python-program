# WAF to return sum of number which is passed in a array.



def sumOfArr(arr):
    sum = 0;
    for x in arr:
        sum = sum +x;
    return (sum)

arr = [1,2,3,4];

print(sumOfArr(arr));
print(sumOfArr([1,2,3,4]));
