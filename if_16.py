# WAP to read percentage of student and print division.
# >=75: 1st class with distinction
# 60-75:1st class
# 50-60:2nd class
# 40-50:3rd class
# <40:fail


a = int(input("enter the marks maths:"));
b = int(input("enter the marks science:"));
c = int(input("enter the marks history:"));
d = int(input("enter the marks english:"));
e = int(input("enter the marks marathi:"));

total_marks = (a + b + c + d + e);
percentage = (total_marks/500) * 100;

if a>=0 and a<=100 and b>=0 and b<=100 and c>=0 and c<=100 and d>=0 and d<=100 and e>=0 and e<=100:

    print("total marks:{}".format(total_marks));
    print("percentage:{}%".format(percentage));

    if a>=40 and b>=40 and c>=40 and d>=40 and e>=40:
        print("passed");
        
        if percentage >= 75:
            print("student passed in 1st class with distinction");
        elif percentage >= 60:
            print("student passed in 1st class");
        elif percentage >= 50:
            print("student passed in 2nd class");
        else:
            print("student passed in 3rd class");
    else:
        print("student is fail");

else:
    print("invalid mark");

