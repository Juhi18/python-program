x = 10; #interger/number
y = 20.4 # float

name = "priyanka" #string
passed = True; # boolean 

print(x)
print(type(x))
print(y)
print(type(y))
print(name)
print(type(name))
print(passed)
print(type(passed))

#concatenation (+)
age = 30
occ = "software engineer"

print(name +" aged "+ str(age) + " is a "+ occ)
print("------------------------------------")
print("{} aged {} is a {}".format(name, age, occ))

